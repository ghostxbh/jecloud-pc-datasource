/**
 * API_URL命名规则：API_模块_方法
 */
//获得产品数据
export const API_SOURCE_GETPRODUCT = '/je/meta/product/load';
//获得资源表树数据
export const API_SOURCE_TREE = '/je/meta/dataSource/getTree';
//添加模块、数据源
export const API_SOURCE_ADD = '/je/meta/dataSource/doSave';
//修改模块、数据源
export const API_SOURCE_UPDATE = '/je/meta/dataSource/doUpdate';
//删除模块、数据源
export const API_SOURCE_REMOVE = '/je/meta/dataSource/doRemove';
//复制数据源
export const API_SOURCE_COPY = '/je/meta/dataSource/doCopy';
//获取数据源单条数据
export const API_SOURCE_SINGLE_DATA = '/je/meta/dataSource/getInfoById';
//树结构移动
export const API_TREE_MOVE = '/je/common/move';
//sql类型根据表id获取DDL视图
export const API_SOURCE_DDL_SQL = '/je/meta/dataSource/getSelectDdlByTableId';
//根据视图配置获取DDL
export const API_SOURCE_CREATE_DDL = '/je/meta/dataSource/getSelectDdlByConfig';
//执行sql
export const API_SOURCE_EXECUTE_SQL = '/je/meta/dataSource/execute';
//执行sql时获取对应的字段
export const API_SOURCE_SQL_FIELDS = '/je/meta/dataSource/getfields';
//获得表格列数据
export const API_TABLE_GETCOLUMNDATA = '/je/meta/resourceTable/table/column/load';
//保存试图数据图表移动坐标位置
export const API_SOURCE_SAVE_POSITION = '/je/meta/dataSource/doUpdate';
//获取对应数据源参数
export const API_SOURCE_PARAMS = '/je/meta/dataSource/getDataSourceParameter';
//获取资源表数据源字段
export const AP_SOURCE_TABLE_FIELDS = '/je/meta/dataSource/getfields';
