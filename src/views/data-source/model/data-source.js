import { ViewModel } from './view-model';
import { JsModel } from './js-model';
import { SqlModel } from './sql-model';
import { ActionModel } from './action-model';
import { ResultConfig } from './result-model';
import { SOURCE_TYPE_ICON } from '@/helper/constant';
// 数据源对象
export class DataSource {
  constructor(options) {
    this.tableCode = 'JE_CORE_DATASOURCE'; //表编码
    this.funcCode = 'JE_CORE_DATASOURCE'; //功能编码
    this.JE_CORE_DATASOURCE_ID = options.JE_CORE_DATASOURCE_ID || ''; //数据源id
    this.DATASOURCE_TEXT = options.DATASOURCE_TEXT || ''; //数据源名称
    this.DATASOURCE_CODE = options.DATASOURCE_CODE || ''; //数据源编码
    this.DATASOURCE_TYPE = options.DATASOURCE_TYPE || ''; //数据源类型
    this.masterTableId = options.masterTableId || ''; //主表id
    this.SY_PRODUCT_ID = options.SY_PRODUCT_ID || ''; //数据源所属于对应产品id
    this.DATASOURCE_DATA = options.DATASOURCE_DATA || ''; //用于存储DDL
    this.DATASOURCE_BASE_TABLE = options.DATASOURCE_BASE_TABLE || ''; //基表，值用逗号分隔
    this.DATASOURCE_OUTPUT_COLUMNS = options.DATASOURCE_OUTPUT_COLUMNS || ''; //输出列(json对象)
    this.DATASOURCE_INCIDENCE_RELATION = options.DATASOURCE_INCIDENCE_RELATION || ''; //关联关系(json对象)
    this.DATASOURCE_RESULT_CONFIG = options.DATASOURCE_RESULT_CONFIG || ''; //结果集(json对象)
    this.DATASOURCE_CONFIG = options.DATASOURCE_CONFIG || ''; //配置,所有类型公共配置(json对象)
  }
}

class OldDataSource {
  constructor(options) {
    this.name = options.name || ''; //数据源名称
    this.code = options.code || ''; //数据源编码
    this.type = options.type || ''; //数据源类型
    this.typeName = options.typeName || ''; //数据源类型名称
    this.productId = options.productId || ''; //对应产品id
    this.productName = options.productName || ''; //对应产品名称
    this.productCode = options.productCode || ''; //对应产品code
    switch (this.type) {
      case SOURCE_TYPE_ICON.viewType: //图形模式
        this.config = options.config ? new ViewModel(options.config) : {};
        break;
      case SOURCE_TYPE_ICON.jsType: //js模式
        this.config = options.config ? new JsModel(options.config) : {};
        break;
      case SOURCE_TYPE_ICON.sqlType: //sql模式
        this.config = options.config ? new SqlModel(options.config) : {};
        break;
      case SOURCE_TYPE_ICON.actionType: //action模式
        this.config = options.config ? new ActionModel(options.config) : {};
        break;
      default:
        break;
    }
    //结果集配置项
    this.resultConfig = options.resultConfig ? new ResultConfig(options.resultConfig) : {};
  }
}
