//Action模式
export class ActionModel {
  constructor(options) {
    this.apiUrl = options.apiUrl; //数据接口服务器地址
    this.fieldName = options.fieldName; //字段名，用于图表报表规划使用，以 逗号 分隔
    this.paramName = options.paramName; //参数名，用于图表报表规划和后台接收参数使用，以 逗号 分隔
    this.resultExplain = options.resultExplain; //返回数据格式说明
  }
}
