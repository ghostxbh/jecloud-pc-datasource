//视图模式
export class ViewModel {
  constructor(options) {
    //视图设计
    this.viewConfig = options.viewConfig?.map((item) => {
      return new ViewField(item);
    });
    //输出视图
    this.outputConfig = options.outputConfig?.map((item) => {
      return new OutputField(item);
    });
    //DDL视图
    this.ddlConfig = options.ddlConfig || '';
    //关联设计
    this.RelatedConfig = options.RelatedConfig?.map((item) => {
      return new RelatedField(item);
    });
    //查询配置
    this.queryConfig = new QueryConfig(options.queryConfig);
  }
}

//视图设计关联表
class ViewField {
  constructor(options) {
    this.tableId = options.tableId; //表id
    this.tableCode = options.tableCode; //表code
    this.tableName = options.tableName; //表名称
    this.iconCls = options.iconCls; //表对应类型的图标icon-class
    this.top = options.top; //该表在视图设计中对应的位置-上
    this.left = options.left; //该表在视图设计中对应的位置-左
  }
}
//输出视图对应字段
class OutputField {
  constructor(options) {
    this.tableCode = options.tableCode; //表对应的code
    this.colomn = options.colomn; //列-对应表中的字段
    this.name = options.name; //名称
    this.value = options.value; //值，例:表(code).字段(code)
    this.dataType = options.dataType; //类型
    this.length = options.length; //长度
    this.grouping = options.grouping || false; //分组，默认不分组
    this.formula = options.formula; //公式: Sum|Avg|Min|Max|Count
    this.sort = options.sort; //排序(升序|降序)
    this.whereColum = options.whereColum; //条件值
  }
}
//关联设计对应字段
class RelatedField {
  constructor(options) {
    this.mainTableCode = options.mainTableCode; //表Acode
    this.mainTableId = options.mainTableId; //表A Id
    this.mainTableName = options.mainTableName; //表A Name
    this.mainColumn = options.mainColumn; //表A字段code
    this.mainIdentifier = options.mainIdentifier; //标识A
    this.targetTableCode = options.aa; //表Bcode
    this.targetTableId = options.aa; //表B Id
    this.targetTableName = options.aa; //表B Name
    this.targetColumn = options.aa; //表Acode
    this.targetIdentifier = options.aa; //表B标识
    this.formula = options.formula; // 操作符 =
    this.isDispalyLine = options.isDispalyLine; //是否显示 1显示 0不显示
  }
}
//关联查询配置
class QueryConfig {
  constructor(options) {
    this.queryFn = options.queryFn || ''; //SQL拼装器(自定义条件)
    this.orderBy = options.orderBy; //排序条件
    this.totalSize = options.totalSize; //取前多少条数据，默认空字符串为所有
    this.getFrontSize = options.getFrontSize; //取前...单位...数据的条数
    this.getFrontUnit = options.getFrontUnit; //取前...单位...数据的单位
    this.baseField = options.baseField || 'SYS_CREATETIME'; //基准字段，默认：SYS_CREATETIME
    this.fieldType = options.baseField || '日期'; //字段类型 日期|日期时间|日期月份, 默认：日期
    this.queryParams = options.queryParams?.map((item) => {
      return new queryField(item);
    });
  }
}
//关联查询查询条件对应字段
class queryField {
  constructor(options) {
    this.code = options.code; //对应的字段名,例：a
    this.type = options.type; //查询条件符号 例：'=',
    this.value = options.value; //查询对应的值：例：'1'
    this.cn = options.cn; //查询条件时对应的关系，例and|or
  }
}
