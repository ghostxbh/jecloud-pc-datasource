/**
 * 判断对象是否为空
 * obj {} 没有属性的对象
 */
export const isEmpty = (obj) => Reflect.ownKeys(obj).length === 0 && obj.constructor === Object;
/**
 * 树结构转列表
 */
export const treeToList = (tree, result = [], level = 0) => {
  tree.forEach((node) => {
    result.push(node);
    node.children && treeToList(node.children, result, level + 1);
  });
  return result;
};

/**
 * 深拷贝对象
 * @param {*} obj
 * @returns
 */
export const deepClone = (data) => {
  //string,number,bool,null,undefined,symbol
  //object,array,date
  if (data && typeof data === 'object') {
    //针对函数的拷贝
    if (typeof data === 'function') {
      let tempFunc = data.bind(null);
      tempFunc.prototype = deepClone(data.prototype);
      return tempFunc;
    }
    switch (Object.prototype.toString.call(data)) {
      case '[object String]':
        return data.toString();
      case '[object Number]':
        return Number(data.toString());
      case '[object Boolean]':
        return new Boolean(data.toString());
      case '[object Date]':
        return new Date(data.getTime());
      case '[object Array]':
        var arr = [];
        for (let i = 0; i < data.length; i++) {
          arr[i] = deepClone(data[i]);
        }
        return arr;

      //js自带对象或用户自定义类实例
      case '[object Object]':
        var obj = {};
        for (let key in data) {
          //会遍历原型链上的属性方法，可以用hasOwnProperty来控制 （obj.hasOwnProperty(prop)
          obj[key] = deepClone(data[key]);
        }
        return obj;
    }
  } else {
    //string,number,bool,null,undefined,symbol
    return data;
  }
};
