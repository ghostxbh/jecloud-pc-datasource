/**
 * 系统常量配置文件，命名规则
 * 平台核心：JE_模块_变量名
 * 业务常量：业务(MENU)_模块_变量名
 */

//数据源视图模式
export const SOURCE_VIEW_OPTIONS = [
  { name: '设计视图', code: 'DESIGN_VIEW' },
  { name: '关联设计', code: 'CONNECT_VIEW' },
  { name: '查询配置', code: 'QUERY_VIEW' },
  { name: '输出视图', code: 'OUTPUT_VIEW' },
  { name: 'DML视图', code: 'DDL_VIEW' },
];
//图形下的模块分类
export const VIEW_OPTION_TYPE = {
  DESIGN_VIEW: 'DESIGN_VIEW',
  CONNECT_VIEW: 'CONNECT_VIEW',
  QUERY_VIEW: 'QUERY_VIEW',
  OUTPUT_VIEW: 'OUTPUT_VIEW',
  DDL_VIEW: 'DDL_VIEW',
};
//资源表数据源下的模式
export const SOURCE_TABLE_OPTIONS = [
  { name: '数据配置', code: 'CONFIG_TABLE' },
  { name: 'DML视图', code: 'DML_TABLE' },
];
//资源表数据源下的模块分类
export const TABLE_OPTION_TYPE = {
  CONFIG_TABLE: 'CONFIG_TABLE',
  DML_TABLE: 'DML_TABLE',
};
//图形-视图设计-关联设计
export const VIEW_DESIGN_RELEVANCY_DESIGN = {
  name: '关联设计',
  code: 'DESIGN_RELEVANCY_DESIGN',
};
//图形-视图设计-查询配置
export const VIEW_DESIGN_SEARCH_CONFIG = {
  name: '查询配置',
  code: 'DESIGN_SEARCH_CONFIG',
};
//切换产品事件
export const BI_CHANGE_PRODUCT = 'BI_CHANGE_PRODUCT';
//数据源-选择对应数据源事件
export const SOURCE_TREE_SELECT = 'SOURCE_TREE_SELECT';
//新建数据源后立即打开当前数据源
export const SOURCE_TREE_SELECT_OPEN = 'SOURCE_TREE_SELECT_OPEN';
//触发产品下的节点数选中
export const SOURCE_SELECT_TREE_NODE = 'SOURCE_SELECT_TREE_NODE';
//重新加载左侧资源树
export const SOURCE_RELOAD_TREE = 'SOURCE_RELOAD_TREE';
//保存时变更资源表名称
export const SOURCE_UPDATE_NAME = 'SOURCE_UPDATE_NAME';
//删除数据源
export const SOURCE_DELETE_SOURCE = 'SOURCE_DELETE_SOURCE';
//复原数据源
export const SOURCE_START_RECOVER = 'SOURCE_START_RECOVER';
//关闭数据源tab事件
export const SOURCE_CLOSE_PANE = 'SOURCE_CLOSE_PANE';
//数据源对象，包括类型及对应icon-class
export const SOURCE_TYPE_ICON = {
  viewType: 'VIEW',
  viewIcon: 'jeicon jeicon-process',
  jsType: 'JS',
  jsIcon: 'jeicon jeicon-js',
  sqlType: 'SQL',
  sqlIcon: 'jeicon jeicon-data',
  actionType: 'ACTION',
  actionIcon: 'jeicon jeicon-datainterface',
  serviceType: 'SERVICE',
  serviceIcon: 'fal fa-share-alt',
  tableType: 'TABLE',
  tableIcon: 'jeicon jeicon-table',
};
//当前数据源信息标识
export const SOURCE_DATA_LABEL = 'SOURCE_DATA_LABEL';
//视图设计添加表
export const SOURCE_VIEW_DESIGN_ADD = 'SOURCE_VIEW_DESIGN_ADD';
//图形模式开始保存数据
export const START_SAVE_SOURCE_VIEW_DATA = 'START_SAVE_SOURCE_VIEW_DATA';
//图形模式正在保存数据
export const SAVE_SOURCE_VIEW_DATA = 'SAVE_SOURCE_VIEW_DATA';

//时间选择单位
export const SOURCE_VIEW_QUERY_TIME = [
  { label: '天', value: 'day' },
  { label: '周', value: 'week' },
  { label: '月', value: 'month' },
  { label: '年', value: 'year' },
  { label: '小时', value: 'hour' },
];
//字段类型-日期选择
export const SOURCE_VIEW_QUERY_DATE = [
  { label: '日期', value: 'date' },
  { label: '日期时间', value: 'date-time' },
  { label: '日期月份', value: 'date-month' },
];
//数据源执行条数
export const SOURCE_EXEC_SIZES = [
  {
    value: 10,
    lable: '10',
  },
  {
    value: 20,
    lable: '20',
  },
  {
    value: 30,
    lable: '30',
  },
  {
    value: 40,
    lable: '40',
  },
  {
    value: 50,
    lable: '50',
  },
  {
    value: 60,
    lable: '60',
  },
  {
    value: 70,
    lable: '70',
  },
  {
    value: 80,
    lable: '80',
  },
  {
    value: 90,
    lable: '90',
  },
  {
    value: 100,
    lable: '100',
  },
];

//数据源类型对应图标
export const SourceTypeAndIcon = [
  { type: 'VIEW', icon: 'jeicon jeicon-process' },
  { type: 'JS', icon: 'jeicon jeicon-js' },
  { type: 'SQL', icon: 'jeicon jeicon-data' },
  { type: 'ACTION', icon: 'jeicon jeicon-datainterface' },
  { type: 'SERVICE', icon: 'fal fa-share-alt' },
  { type: 'TABLE', icon: 'jeicon jeicon-table' },
];
